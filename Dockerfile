FROM quay.io/loryza/nersc-mpich:latest

RUN cd /tmp && wget https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    pip install --no-cache-dir setuptools && \
    pip install --no-cache-dir sympy && \
    pip install --no-cache-dir numpy && \
    pip install --no-cache-dir ply && \
    pip install --no-cache-dir six

RUN cd /build && git clone https://github.com/jbornschein/mpi4py-examples.git
